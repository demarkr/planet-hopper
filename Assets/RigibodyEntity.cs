using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigibodyEntity : PhysicsObject {
    public PhysicsObject parentPlanet;

    public float floorContactRange = 0.2f;
    public float floorOffset = 0.5f;
    [SerializeField]
    private LayerMask groundLayerMask;
    [SerializeField]
    private float groundingLerpSpeed = 2f;

    private bool grounded = false;

    public bool isGrounded {
        get { return grounded; }
    }

    private Vector2 directionToPlanet {
        get { return (parentPlanet.getCenterOfMass() - getCenterOfMass()).normalized; }
    }

    private Rigidbody2D rb2d;
    private void Awake() {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        getClosestPlanet();
        if (!parentPlanet)return;

        if (!grounded) {
            lerpTransformDownDirection(-directionToPlanet);
        }
    }

    private void FixedUpdate() {
        if (!parentPlanet)return;

        // Move towards planet
        rb2d.velocity = GetAcceleration(GetForceUsingObject(parentPlanet), grounded);
    }

    private void getClosestPlanet() {
        // Get the closest planet
        //parentPlanet = PlanetManager.GetInstance().GetClosestPlanet(transform.position);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        // If the collider is in the ground layers
        if (groundLayerMask == (groundLayerMask | (1 << other.gameObject.layer))) {
            grounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other) {
        // If the collider is in the ground layers
        if (groundLayerMask == (groundLayerMask | (1 << other.gameObject.layer))) {
            grounded = false;
        }
    }

    public void lerpTransformDownDirection(Vector2 newDownDirection) {
        // Make sure our down direction is pointing at the planet
        transform.up = Vector2.Lerp(
            transform.up, newDownDirection,
            groundingLerpSpeed * Time.deltaTime);
    }

    private void OnDrawGizmos() {
        Vector3 virtualGroundPosition = transform.position - transform.up * floorContactRange;
        // Draw ground check ray
        Gizmos.color = new Color(0.2f, 0.2f, 0, 0.2f);
        Gizmos.DrawLine(transform.position, virtualGroundPosition);
    }
}