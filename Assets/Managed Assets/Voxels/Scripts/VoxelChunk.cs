using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class VoxelChunk : MonoBehaviour {
    private enum eEdge { top, left, bottom, right }

    public float colliderDepth = 0.1f;

    private VoxelChunkSettings settings = null;
    private SpriteRenderer spriteRenderer;
    private MeshCollider meshCollider;

    // private List<EdgeQuad> edgeQuads = new List<EdgeQuad>();

    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();

    public void UpdateChunk() {
        if (this.settings == null)return;

        if (!meshCollider)meshCollider = GetComponentInChildren<MeshCollider>();
        if (!spriteRenderer)spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = settings.getSprite;
        meshCollider.transform.localScale = spriteRenderer.bounds.size.setZValue(1);
        //t2d.backCollider.localScale = new Vector3(t2d.bc2d.size.x, t2d.bc2d.size.y, 1);

        GenerateMesh();
    }
    int width, height;
    public void SetSettings(VoxelChunkSettings settings) {
        this.settings = settings;

        // TODO: Remove this.
        this.width = settings.getWidth;
        this.height = settings.getHeight;
    }

    public VoxelChunkSettings GetSettings() {
        return settings;
    }

    Task generateMeshTask;
    public void GenerateAsync() {
        generateMeshTask = new Task(GenerateMesh);
        generateMeshTask.Start();
    }

    public void GenerateMesh() {
        // Clear everything
        vertices = new List<Vector3>();
        normals = new List<Vector3>();

        findEdges();

        if (vertices.Count > 65000) { Debug.LogError("Too many quad vertices!"); return; }

        // Setup the mesh
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.normals = normals.ToArray();

        // Set indices, array from 0 to allVertices.count
        int[] indices = new int[vertices.Count];
        for (int i = 0; i < indices.Length; i++)
            indices[i] = i;

        mesh.SetIndices(indices, MeshTopology.Quads, submesh : 0);
        mesh.SetTriangles(mesh.GetTriangles(0), submesh : 0);
        meshCollider.sharedMesh = mesh;

        //spriteRenderer.enabled = vertices.Count > 3;
    }

    private class Edge {
        public Vector3 point1, point2, depth, normalDirection;
        public Edge(Vector3 point1, Vector3 point2, Vector3 depth, Vector3 normalDirection) {
            this.point1 = point1;
            this.point2 = point2;
            this.depth = depth;
            this.normalDirection = normalDirection;
        }
    }

    private void findEdges() {
        List<Edge> edges = new List<Edge>();
        // Loop through all pixels, to find the edge pixels
        // The edge is the collider
        for (var y = 0; y < settings.getHeight; y++) {
            for (int x = 0; x < settings.getWidth; x++) {
                if (HasPixel(x, y)) {
                    if (x == 0 || !HasPixel(x - 1, y))
                        edges.Add(AddEdge(x, y, eEdge.left));

                    if (x == settings.getWidth - 1 || !HasPixel(x + 1, y))
                        edges.Add(AddEdge(x, y, eEdge.right));

                    if (y == 0 || !HasPixel(x, y - 1))
                        edges.Add(AddEdge(x, y, eEdge.bottom));

                    if (y == settings.getHeight - 1 || !HasPixel(x, y + 1))
                        edges.Add(AddEdge(x, y, eEdge.top));
                }
            }
        }

        edges.ForEach(edge => AddQuad(edge));
    }

    private Edge AddEdge(int x, int y, eEdge edgeSide) {
        Vector3 normalDirection = Vector3.up;

        Vector2 singlePixelSize = new Vector2(1.0f / settings.getWidth, 1.0f / settings.getHeight);
        Vector2 pixelPosition = new Vector3(x * singlePixelSize.x, y * singlePixelSize.y);

        // Set vertex points
        Vector2 point1 = pixelPosition - Vector2.one * 0.5f;
        Vector2 point2 = point1;

        switch (edgeSide) {
            case eEdge.top:
                point1 += singlePixelSize; // Top right
                point2.y += singlePixelSize.y; // Top left
                normalDirection = Vector3.up;
                break;

            case eEdge.bottom:
                point2.x += singlePixelSize.x; // Bottom right
                normalDirection = Vector3.down;
                break;

            case eEdge.left:
                point1.y += singlePixelSize.y; // Top left
                normalDirection = Vector3.left;
                break;

            case eEdge.right:
                point1.x += singlePixelSize.x; // Bottom right
                point2 += singlePixelSize; // Top right
                normalDirection = Vector3.right;
                break;
        }

        return new Edge(point1,
            point2,
            depth : Vector3.forward * colliderDepth,
            normalDirection);
        // Add the edge to the list of edge quads
        // AddQuad(
        //     point1,
        //     point2,
        //     depth : Vector3.forward * colliderDepth,
        //     normalDirection
        // );
    }

    private void AddQuad(Edge edge) {
        // Add all the vertices of the edge
        vertices.Add(edge.point1);
        vertices.Add(edge.point2);
        vertices.Add(edge.point2 + edge.depth);
        vertices.Add(edge.point1 + edge.depth);

        // For each verticie add a normal as well
        normals.Add(edge.normalDirection);
        normals.Add(edge.normalDirection);
        normals.Add(edge.normalDirection);
        normals.Add(edge.normalDirection);
    }

    private bool HasPixel(int x, int y) {
        // If the alpha is above 50, pixel exists
        return settings.getPixels[x + y * width].a > 50;
    }
}