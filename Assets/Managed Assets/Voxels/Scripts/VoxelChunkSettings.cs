using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelChunkSettings {
    private Texture2D texture;
    private Sprite sprite;
    private Color32[] pixels;
    private int width, height;

    public VoxelChunkSettings(Texture2D texture, Sprite sprite) {
        this.texture = texture;
        this.sprite = sprite;
        this.pixels = texture.GetPixels32();
        this.width = texture.width;
        this.height = texture.height;
    }

    // Getters
    public Texture2D getTexture { get { return this.texture; } }

    public Sprite getSprite { get { return this.sprite; } }

    public Color32[] getPixels { get { return this.pixels; } }

    public int getWidth { get { return this.width; } }

    public int getHeight { get { return this.height; } }

    public Vector2 getDimensions() {
        return new Vector2(getWidth, getHeight);
    }

    public void deletePixel(int x, int y) {
        this.texture.SetPixel(x, y, Color.clear);

        // Set the pixel to clear in the pixesls array aswell
        int pixelIndex = y * getWidth + x;
        pixels[pixelIndex] = Color.clear;

        texture.Apply();
    }
}