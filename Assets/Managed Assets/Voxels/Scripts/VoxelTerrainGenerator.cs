using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class VoxelTerrainGenerator : MonoBehaviour {
    [BoxGroup("Parameters")]
    public Vector3 scale = Vector3.one;
    [BoxGroup("Parameters")]
    public int pixelsPerUnit = 10;
    [BoxGroup("Parameters")]
    public Vector2 terrainDivisions;

    [PreviewField(64, ObjectFieldAlignment.Left), BoxGroup("Assets")]
    public Texture2D terrainTexture;
    [BoxGroup("Assets")]
    public GameObject terrainChunkPrefab;

    [BoxGroup("Digging")]
    public LayerMask diggingLayers;

    private static VoxelTerrainGenerator instance;
    private void Awake() {
        instance = this;
    }

    public static VoxelTerrainGenerator GetInstance() {
        return instance;
    }

    private void Start() {
        ClearTerrain();
        GenerateTerrain();
    }

    [Button("Generate")]
    private void editorGenerate() {
        ClearTerrain();
        GenerateTerrain();
    }

    public void GenerateTerrain() {
        // If the divisons set are not exact
        if (!isExactDivions()) {
            Debug.LogError($"Resolution not dividable by {terrainDivisions.ToString()}");
            return;
        }

        for (int xIndex = 0; xIndex < terrainDivisions.x; xIndex++) {
            for (var yIndex = 0; yIndex < terrainDivisions.y; yIndex++) {
                // Set transform settings
                Transform chunkTransform = Instantiate(terrainChunkPrefab, Vector3.zero, Quaternion.identity).transform;
                chunkTransform.name = $"Chunk - {xIndex.ToString()} , {yIndex.ToString()}";
                chunkTransform.parent = this.transform;

                chunkTransform.transform.localPosition = chunkToWorldSpace(xIndex, yIndex);
                chunkTransform.localScale = scale;

                // Set chunk settings
                VoxelChunk voxelChunk = chunkTransform.GetComponent<VoxelChunk>();

                // Create the texture used for the sprite-renderer, with the size of a single chunk
                Vector2 singleChunkSize = getSingleChunkSize();
                Texture2D chunkTexture = new Texture2D((int)singleChunkSize.x, (int)singleChunkSize.y);
                chunkTexture.filterMode = FilterMode.Point;

                // Get the pixels corresponding to the chunk, from the main texture
                Color[] chunkPixels = terrainTexture.GetPixels(
                    x: (int)(xIndex * singleChunkSize.x),
                    y: (int)(yIndex * singleChunkSize.y),
                    blockWidth: (int)singleChunkSize.x,
                    blockHeight: (int)singleChunkSize.y
                );
                chunkTexture.wrapMode = TextureWrapMode.Clamp;

                // Set the pixels
                chunkTexture.SetPixels(chunkPixels);
                chunkTexture.Apply();

                // Create the sprite, to use the previously created texture
                Sprite chunkSprite = Sprite.Create(
                    chunkTexture,
                    new Rect(0, 0, chunkTexture.width, chunkTexture.height),
                    pivot : new Vector2(0.5f, 0.5f),
                    pixelsPerUnit
                );

                voxelChunk.SetSettings(new VoxelChunkSettings(chunkTexture, chunkSprite));
                voxelChunk.UpdateChunk();
            }
        }
    }

    public void Dig(Vector3 position, int radius) {
        // Find which chunk we want to dig, using raycast
        RaycastHit hit;
        Debug.DrawRay(position.setZValue(-10), transform.forward * 1000, Color.yellow, 5);
        Physics.Raycast(position.setZValue(-10), transform.forward, out hit, 2000, diggingLayers);
        print(hit.transform?.name);
        VoxelChunk targetChunk = hit.transform?.parent!.parent!.GetComponent<VoxelChunk>();
        if (!targetChunk)return;

        Vector2 textureCoordinate = hit.textureCoord;
        Vector2 chunkTextureSize = targetChunk.GetSettings().getDimensions();

        // Convert the texture coordinate to the actual pixel position
        // Relative to the chunk
        Vector2 pixelPosition = new Vector2(
            textureCoordinate.x * chunkTextureSize.x,
            textureCoordinate.y * chunkTextureSize.y
        );

        int l = (int)chunkTextureSize.x * (int)chunkTextureSize.y;

        for (int y = -radius; y <= radius; y++) {
            for (int x = -radius; x <= radius; x++) {
                Vector2 offsetPixelPosition = pixelPosition + new Vector2(x, y); //Add the current radius check index offset

                if (Vector2.Distance(offsetPixelPosition, pixelPosition) <= radius) {
                // Pixel position is in bounds
                if (offsetPixelPosition.x >= 0 && offsetPixelPosition.x <= chunkTextureSize.x &&
                    offsetPixelPosition.y >= 0 && offsetPixelPosition.y <= chunkTextureSize.y) {
                    targetChunk.GetSettings().deletePixel((int)offsetPixelPosition.x, (int)offsetPixelPosition.y);
                    targetChunk.GenerateMesh();
                }
                }
            }
        }

    }

    [Button("Clear Voxels")]
    public void ClearTerrain() {
        while (transform.childCount > 0) {
            DestroyImmediate(transform.GetChild(0).gameObject, false);
        }
    }

    private Vector2 getSingleChunkSize() {
        return new Vector2(
            terrainTexture.width / terrainDivisions.x,
            terrainTexture.height / terrainDivisions.y
        );
    }

    private Vector2 chunkToWorldSpace(int chunkXIndex, int chunkYIndex) {
        // TODO: Maybe add extra division for PixelPerUnit
        return new Vector2(
            chunkXIndex * terrainTexture.width / terrainDivisions.x / pixelsPerUnit,
            chunkYIndex * terrainTexture.height / terrainDivisions.y / pixelsPerUnit
        );
    }

    private bool isExactDivions() {
        return (terrainTexture.width % terrainDivisions.x + terrainTexture.height % terrainDivisions.y) == 0;
    }

    [Button("Get Even Dividers", ButtonSizes.Small, ButtonStyle.CompactBox)]
    private void getEvenDivisions() {
        if (!terrainTexture) { Debug.LogError("No Texture!"); return; }

        string dividersX = "";
        string dividersY = "";

        for (int i = 1; i < terrainTexture.height; i++) {
            if ((terrainTexture.height % i) == 0)
                dividersY += $"{i.ToString()},";
        }

        for (int i = 1; i < terrainTexture.width; i++) {
            if ((terrainTexture.width % i) == 0)
                dividersX += $"{i.ToString()},";
        }

        Debug.Log($" * Even dividers width: {dividersX}");
        Debug.Log($" * Even dividers height: {dividersY}");
    }
}