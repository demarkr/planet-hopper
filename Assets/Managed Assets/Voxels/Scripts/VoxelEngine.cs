using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class VoxelEngine : MonoBehaviour {
    public GameObject voxelPrefab;
    public Vector2 center = Vector2.zero;
    [MinValue(1)]
    public int radius = 10;
    [MinValue(0.01f), MaxValue(5f)]
    public float resolutionScale = 1f;

    void Start() {
        center = transform.position;
        GenerateCircle();
    }

    [Button("Genreate Circle")]
    public void GenerateCircle() {
        ClearVoxels();

        for (float y = -radius; y <= radius; y += resolutionScale) {
            for (float x = -radius; x <= radius; x += resolutionScale) {
                Vector2 voxelPoint = new Vector2(x, y);
                float distanceToCircleCenter = Vector2.Distance(voxelPoint, Vector2.zero);

                if (distanceToCircleCenter <= radius) {
                    //TODO: Use object-pooling
                    GameObject voxel = Instantiate(voxelPrefab, center + voxelPoint, Quaternion.identity, parent : this.transform);
                    voxel.transform.localScale = Helpers.vectorFromFloat(resolutionScale);
                }
            }
        }

    }

    [Button("Clear Voxels")]
    public void ClearVoxels() {
        while (transform.childCount > 0) {
            DestroyImmediate(transform.GetChild(0).gameObject, false);
        }
    }
}