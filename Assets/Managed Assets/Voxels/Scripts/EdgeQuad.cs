using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeQuad {
    private List<Vector3> vertices = new List<Vector3>();
    private List<Vector3> normals = new List<Vector3>();

    public EdgeQuad(Vector3 point1, Vector3 point2, Vector3 depth, Vector3 normalDirection) {
        // Add all the vertices of the edge
        vertices.Add(point1);
        vertices.Add(point2);
        vertices.Add(point2 + depth);
        vertices.Add(point1 + depth);

        // For each verticie add a normal as well
        vertices.ForEach((v) => normals.Add(normalDirection));
    }

    public List<Vector3> getVertices() {
        return this.vertices;
    }

    public List<Vector3> getNormals() {
        return this.normals;
    }
}