using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace MarkR {
    public class CameraController : MonoBehaviour {
        [Range(0, 50), SerializeField]
        private float followSpeed = 8;

        [SerializeField]
        private int CameraZ = -10;

        private void Update() {
            RigibodyEntity playerInstance = PlayerSpawner.GetInstance().GetPlayerInstance();
            if (playerInstance)
                LerpCamera(playerInstance.transform);
        }

        private void LerpCamera(Transform target) {
            // Lerp camera to target
            transform.position = Vector3.Lerp(
                (Vector2)transform.position,
                (Vector2)target.position,
                followSpeed * Time.deltaTime).setZValue(CameraZ);
        }
    }
}