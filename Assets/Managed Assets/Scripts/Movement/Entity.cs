using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : PhysicsObject {
    private PhysicsObject parentPlanet;

    // Grounded
    public float floorContactRange = 0.2f;
    public float floorOffset = 0.5f;
    [SerializeField]
    private LayerMask groundLayerMask;
    [SerializeField]
    private float groundingLerpSpeed = 2f;
    private bool grounded = false;
    public bool isGrounded {
        get { return grounded; }
    }

    private Vector2 directionToPlanet {
        get { return (parentPlanet.getCenterOfMass() - getCenterOfMass()).normalized; }
    }

    private void Update() {
        updateParentPlanet();
        if (!parentPlanet)return;

        checkFloorContact();
        if (!grounded) {
            lerpTransformDownDirection(-directionToPlanet);
        }
    }

    private void FixedUpdate() {
        if (!parentPlanet)return;

        // Move towards planet
        transform.position =
            (Vector2)transform.position + GetAcceleration(GetForceUsingObject(parentPlanet), grounded) * Time.deltaTime;
    }

    private void updateParentPlanet() {
        parentPlanet = PlanetManager.GetInstance().GetClosestPlanet(transform.position);
    }

    private void checkFloorContact() {
        RaycastHit2D? floorContactPoint = floorContact(floorContactRange);
        grounded = floorContactPoint.HasValue;
        if (floorContactPoint.HasValue) {
            // Offset floor-contact by floorOffset
            transform.position = Vector3.Lerp(
                transform.position,
                (Vector3)(floorContactPoint.Value.point * floorOffset),
                groundingLerpSpeed * Time.deltaTime
            );

            // Apply directional change
            Vector2 directionToContact = -floorContactPoint.Value.normal.normalized;
            lerpTransformDownDirection(-directionToContact);
        }
    }

    public void lerpTransformDownDirection(Vector2 newDownDirection) {
        transform.up = Vector2.Lerp(
            transform.up, newDownDirection,
            groundingLerpSpeed * Time.deltaTime);
    }

    public RaycastHit2D? floorContact(float range = 0.2f) {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, directionToPlanet, range, groundLayerMask);
        return hit.transform ? hit : null;
    }

    private void OnDrawGizmos() {
        Vector3 virtualGroundPosition = transform.position - transform.up * floorContactRange;
        // Draw ground check ray
        Gizmos.color = new Color(0.2f, 0.2f, 0, 0.2f);
        Gizmos.DrawLine(transform.position, virtualGroundPosition);
    }
}