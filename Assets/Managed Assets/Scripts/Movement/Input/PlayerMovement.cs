using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float movementSpeed = 1;
    public float jumpVelocity = 10;

    // Steps check - Sideways ray
    [MinValue(0), InfoBox("Ray length for both obstacle checks, ground-level/clearance")]
    public float obstacleRayLength = 0.2f;
    public float obstacleRayOffset = 0.01f;
    public float obstacleClearanceRayOffset = 0.5f;
    [MinValue(0)]
    public float obstacleJumpForce = .5f;
    public LayerMask obstacleLayers;

    private RigibodyEntity entity;
    private void Awake() {
        entity = GetComponent<RigibodyEntity>();
    }

    private void Update() {
        float xMovement = Input.GetAxisRaw("Horizontal") * movementSpeed;
        Vector3 upwardsForce = Vector2.zero;

        if (xMovement != 0) {
            // Obstacle check at ground-level
            var obstacleCheckGround = obstacleCheck(obstacleRayOffset);
            var obstacleCheckClearance = obstacleCheck(obstacleClearanceRayOffset);

            // If we hit a ground obstacle, but the clearance ray didn't hit anything
            if ((obstacleCheckGround.right && !obstacleCheckClearance.right) || (obstacleCheckGround.left && !obstacleCheckClearance.left)) {
                upwardsForce = transform.up * obstacleJumpForce;
            }
        }

        entity.AddVelocity((transform.right + upwardsForce) * 10 * xMovement * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space)) {
            entity.AddVelocity(transform.up * jumpVelocity);
        }
    }

    private(bool left, bool right)obstacleCheck(float offset) {
        Vector2 rayOrigin = transform.position + transform.up * offset;
        bool rightCheck = obstacleRay(rayOrigin, transform.right);
        bool leftCheck = obstacleRay(rayOrigin, -transform.right);
        return (leftCheck, rightCheck);
    }

    private RaycastHit2D obstacleRay(Vector2 origin, Vector2 direction) {
        return Physics2D.Raycast(origin, direction, obstacleRayLength, obstacleLayers);
    }

    private void OnDrawGizmosSelected() {
        // Draw ground ray
        Gizmos.color = new Color(0, 0, 1, 0.5f);
        Gizmos.DrawRay(transform.position + transform.up * obstacleRayOffset, transform.right * obstacleRayLength);
        Gizmos.DrawRay(transform.position + transform.up * obstacleRayOffset, -transform.right * obstacleRayLength);

        // Draw clearance ray
        Gizmos.DrawRay(transform.position + transform.up * obstacleClearanceRayOffset, transform.right * obstacleRayLength);
        Gizmos.DrawRay(transform.position + transform.up * obstacleClearanceRayOffset, -transform.right * obstacleRayLength);
    }
}