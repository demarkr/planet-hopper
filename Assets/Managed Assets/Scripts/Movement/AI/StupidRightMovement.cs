using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StupidRightMovement : MonoBehaviour {
    public float movementSpeed = 1;
    public float jumpVelocity = 10;

    private Entity entity;
    private void Awake() {
        entity = GetComponent<Entity>();
    }

    private void Update() {
        float xMovement = Input.GetAxis("Horizontal") * movementSpeed;
        entity.AddVelocity(transform.right * 10 * xMovement * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space)) {
            entity.AddVelocity(transform.up * jumpVelocity);
        }
    }
}