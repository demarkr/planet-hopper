using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour {
    [SerializeField, Range(0.1f, 10000f)]
    private float mass = 1;
    [SerializeField, Range(0.1f, 1f)]
    private float friction = 0.9f;

    public Vector2 GetForceUsingObject(PhysicsObject biggerObject) {
        // See: https://en.wikipedia.org/wiki/Gravitational_acceleration
        float distanceBetweenCenters = Vector2.Distance(getCenterOfMass(), biggerObject.getCenterOfMass());
        Vector2 direction = (getCenterOfMass() - biggerObject.getCenterOfMass()).normalized;
        distanceBetweenCenters *= distanceBetweenCenters;
        return -(((GlobalSettings.GravitationalConstant * biggerObject.getMass()) / distanceBetweenCenters) * direction);
    }

    Vector2 velocity = new Vector2();
    public Vector2 GetAcceleration(Vector2 force, bool grounded = false) {
        velocity *= friction;
        return ((grounded ? Vector2.zero : force) + velocity) / getMass();
    }

    public void AddVelocity(Vector2 amount) {
        velocity += amount;
    }

    public Vector2 getVelocity(){
        return velocity;
    }

    public Vector2 getCenterOfMass() {
        // Rough estimation of center of mass
        // TODO: Maybe rethink calculating the center
        return transform.position;
    }

    public float getMass() {
        return mass;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = new Color(0,1,0.2f,0.4f);
        Gizmos.DrawSphere(getCenterOfMass(), 0.5f);
    }
}