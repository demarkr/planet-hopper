using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlanetManager : MonoBehaviour {
    // Spawning
    [BoxGroup("Populating Planets")]
    [SerializeField, PreviewField(128, ObjectFieldAlignment.Left)]
    private GameObject planetPrefab;

    [BoxGroup("Populating Planets")]
    [Title(""), SerializeField, MinMaxSlider(0, 5000, true)]
    private Vector2 spawnRange;

    [BoxGroup("Populating Planets")]
    [SerializeField, Range(0, 20)]
    private int planetsToSpawn = 4;

    private List<PhysicsObject> spawnedPlanets = new List<PhysicsObject>();
    private static PlanetManager instance;
    private void Awake() {
        instance = this;
    }

    public static PlanetManager GetInstance() {
        return instance;
    }

    private void Start() {
        for (var i = 0; i < planetsToSpawn; i++) {
            SpawnPlanet();
        }
    }

    public void SpawnPlanet() {
        spawnedPlanets.Add(Instantiate(planetPrefab, getRandomSpawnPos(), Quaternion.identity).GetComponent<PhysicsObject>());
    }

    public List<PhysicsObject> GetAllPlanets() {
        return this.spawnedPlanets;
    }

    public PhysicsObject GetClosestPlanet(Vector2 pos) {
        PhysicsObject closestPlanet = null;
        float closestDistance = Mathf.Infinity;

        spawnedPlanets.ForEach(planet => {
            float distance = Vector2.Distance(planet.transform.position, pos);
            if (distance < closestDistance) {
                closestPlanet = planet;
                closestDistance = distance;
            }
        });

        return closestPlanet;
    }

    private Vector2 getRandomSpawnPos() {
        return (Vector2)Helpers.vectorFromFloat(spawnRange.x) + // Must be bigger then spawn inner circle
            Random.insideUnitCircle * spawnRange.y;
    }

    private void OnDrawGizmosSelected() {
        // Draw the max range
        Gizmos.color = new Color(1, 0.5f, 0, 0.3f);
        Gizmos.DrawWireSphere(Vector3.zero, spawnRange.y);

        // Draw the min range
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(Vector3.zero, spawnRange.x);

    }
}