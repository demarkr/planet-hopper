using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {
    [SerializeField, LabelWidth(0.3f)]
    private GameObject playerPrefab;
    [SerializeField, LabelWidth(0.4f), MinMaxSlider(0, 1000, true)]
    private Vector2 spawnRange;

    // TODO: Change Transform to the player class
    private RigibodyEntity playerInstance;

    private static PlayerSpawner instance;
    private void Awake() {
        instance = this;
    }

    public static PlayerSpawner GetInstance() {
        return instance;
    }

    private void Start() {
        checkPlayerExistence();
        SpawnPlayer();
    }

    private void checkPlayerExistence() {
        playerInstance = GameObject.FindGameObjectWithTag("Player").GetComponent<RigibodyEntity>();
    }

    public void SpawnPlayer() {
        if (playerInstance) {
            Debug.LogError("Trying to spawn player twice!");
            Destroy(playerInstance.gameObject);
        }

        playerInstance = Instantiate(playerPrefab, getSpawnPosition(), Quaternion.identity).GetComponent<RigibodyEntity>();
    }

    public RigibodyEntity GetPlayerInstance() {
        return playerInstance;
    }

    private Vector2 getSpawnPosition() {
        return (Vector2)Helpers.vectorFromFloat(spawnRange.x) + // Must be bigger then spawn inner circle
            Random.insideUnitCircle * spawnRange.y;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = new Color(0.5f, 0, 0.5f, 0.3f);
        Gizmos.DrawWireSphere(Vector2.zero, spawnRange.x);

        Gizmos.color = new Color(0f, 0.5f, 0.5f, 0.3f);
        Gizmos.DrawWireSphere(Vector2.zero, spawnRange.y);
    }
}