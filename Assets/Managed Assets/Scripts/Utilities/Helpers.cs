using UnityEngine;

public static class Helpers {
     public static T FindComponentInChildWithTag<T>(this Transform parent, string tag)where T : Component {
          foreach (Transform tr in parent) {
               if (tr.tag == tag) {
                    return tr.GetComponent<T>();
               }
          }
          return null;
     }

     public static Vector2 ClampVector(this Vector2 value, float left, float right, float top, float bottom) {
          if (value.x < left)value.x = left;
          else if (value.x > right)value.x = right;

          if (value.y < bottom)value.y = bottom;
          else if (value.y > top)value.y = top;

          return value;
     }

     public static Vector3 vectorFromFloat(this float value) {
          return new Vector3(value, value, value);
     }

     public static Vector3 setZValue(this Vector3 value, float zValue) {
          return new Vector3(value.x, value.y, zValue);
     }
     public static Vector3 setXValue(this Vector3 value, float xValue) {
          return new Vector3(xValue, value.y, value.z);
     }
     public static Vector3 setYValue(this Vector3 value, float yValue) {
          return new Vector3(value.x, yValue, value.z);
     }

     public static Vector3 subtract(this Vector3 value, float amount) {
          return new Vector3(value.x - amount, value.y - amount, value.z - amount);
     }

     public static Vector2 subtract(this Vector2 value, float amount) {
          return new Vector3(value.x - amount, value.y - amount);
     }

     public static Color RandomColor() {
          return new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
     }

     public static bool RandomBool() {
          return Random.Range(0.0f, 1.0f) > 0.5f;
     }

     public static Vector3 RandomVector2(Vector2 range) {
          return new Vector2(Random.Range(range.x, range.y), Random.Range(range.x, range.y));
     }

     public static Color setAlpha(this Color color, float alpha) {
          return new Color(color.r, color.g, color.b, alpha);
     }

     public static Color modifyColorHue(Color color, float time) {
          float h, s, v;
          Color.RGBToHSV(color, out h, out s, out v);
          h = time % 1;
          return Color.HSVToRGB(h, s, v);
     }

     public static Color setRandomHue(this Color color) {
          float h, s, v;
          Color.RGBToHSV(color, out h, out s, out v);
          h = Random.Range(0.0f, 1.0f);
          return Color.HSVToRGB(h, s, v);
     }

     public static void DestroyAllChildren(this Transform parent) {
          foreach (Transform child in parent) {
               GameObject.Destroy(child.gameObject);
          }
     }
}